<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         '/1VE|mn6B@GV8&m2oH6n|lOblYRAqvA04)@_R*n{`u;R+o.QG+oJ y$5QpDYoZ1N');
define('SECURE_AUTH_KEY',  'wR7#<L|^/I.H1mI l.PdwaU?;Ekyg|:kB !|Uk?%;+<c|~O@Qi1&Z3U*FRR4gd8C');
define('LOGGED_IN_KEY',    '5}NJf@iaf4+pT/Zdj%f ^ef?3 9cwzx?lSs;|DC0jnN+D:>|r8/`tfrc?pHGm K-');
define('NONCE_KEY',        '^&WCs?K7N-(]T]@!7)~*{{W,Q-8d y,Ml6,u2r1!x-Sf*%OWzs%gheowe2cU ^Wv');
define('AUTH_SALT',        '4;ZKDmzGICYovU1|Z,{)U8SU`SdX|G<#Xlz3adI+RU:23K%#bi*EDK|dC[+G^1zy');
define('SECURE_AUTH_SALT', 'z mK{:GLo#acPMr)G[+4p0B$X_*:1I7?(lP^t&+e~?+H[}EZ:OW)oD#%:l1zG_j+');
define('LOGGED_IN_SALT',   '/= HbQ2a,VO)C F^LL%NY6b=>c^m{LrNDV~9@M/,^_o<M*#>|WVS}D^un1fxnaQN');
define('NONCE_SALT',       'o3g))^jQlBQrqh g1z^F9?HTkB:wg7m1>4QI.(shT|!?>!3Q0P-)urJlFOXL4|_f');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
