<!-- header.php -->

<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use ChildTheme\Options\GlobalOptions;
//use ChildTheme\Podcast\PodcastEpisode;
//use ChildTheme\Podcast\PodcastEpisodeRepository;

//$FeaturedPodcastEpisode = GlobalOptions::featuredPodcastEpisode() ?: (new PodcastEpisodeRepository())->findOne([]);
?>

<header class="header-nav sticky-header" data-gtm="Header">
  <div class="navbar">
    <ul>
      <li>Home</li>
      <li>Nino Royalle</li>
      <li>Blog</li>
      <li>Recipes</li>
      <li>Podcast</li>
      <li>Video</li>
      <li>Shop</li>
      <li>Ask</li>
    </ul>
  </div>
<?php //if ($FeaturedPodcastEpisode instanceof PodcastEpisode): ?>
<!-- <div class="header-nav__featured-podcast"> -->
<!-- <div class="header-nav__featured-podcast-container"> -->
<?php //if ($featured_podcast_text = GlobalOptions::featuredPodcastText()): ?>
<!-- <strong class="header-nav__featured-podcast-text"><?php /*Comment of something important.*/ ?></strong> -->
<?php //endif; ?>
<!-- <audio controls> -->
<!-- <source src="<?php //$FeaturedPodcastEpisode->source; ?>"> -->
<!-- Your browser does not support the audio element. -->
<!-- </audio> -->
<!-- </div> -->
<!-- </div> -->
<?php // endif; ?>
<?php if (has_nav_menu('primary_navigation')): ?>
<div class="header-nav__menu">
<?= NavMenuView::createResponsive(); ?>
<button class="header-nav__search-button"><?= new IconView(['icon_name' => 'search']); ?></button>
</div>
<?php endif; ?>
<div class="header-nav__search-drawer">
<?= get_search_form(); ?>
</div>
</header>
