<?php
/**
 * Template Name: Construction
 *
 */
use Backstage\Util;
?>
<div class="container">
  <img src="<?= Util::getAssetPath('images/placeholder200.png'); ?>" />

    <h1 class="heading heading--xlarge" >Our New Site Is Launching Soon</h1>

      <p>in the meantime please connect via social media or fill out the form below to have one of our agents reach out to you.</p>
        <div class="socials">
          <ul>
            <li>
              <!-- email -->
              <img src="<?= Util::getAssetPath('images/email.png'); ?>" />
            </li>
            <li>
              <!-- facebook -->
              <img src="<?= Util::getAssetPath('images/facebook.png'); ?>" />
            </li>
            <li>
              <!-- twitter -->
              <img src="<?= Util::getAssetPath('images/twitter.png'); ?>" />
            </li>
            <li>
              <!-- pinterest -->
              <img src="<?= Util::getAssetPath('images/pinterest.png'); ?>" />
            </li>
            <li>
              <!-- instagram -->
              <img src="<?= Util::getAssetPath('images/instagram.png'); ?>" />
            </li>
            <li>
              <!-- youtube -->
              <img src="<?= Util::getAssetPath('images/youtube.png'); ?>" />
            </li>
          </ul>
        </div>
</div>
